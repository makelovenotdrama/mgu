import torch.nn as nn
import torchvision

num_classes = 10

def create_resnet():
    resnet = torchvision.models.resnet18(pretrained = True)
        
    #for param in resnet.parameters():
    #    param.requires_grad = False

    num_features = resnet.fc.in_features
    resnet.fc = nn.Linear(num_features, num_classes)
    return resnet