import torch.nn as nn
import torch.nn.functional as F
class ResNetSm(nn.Module):
    def __init__(self):
        super(ResNetSm, self).__init__()
        self.begFilter = nn.Sequential(
            nn.Conv2d(3, 32, 3, padding = 1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.Conv2d(32, 32, 3, padding = 1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(2, 2),
            nn.Dropout2d(p = 0.2)
        )
        self.block1 = nn.Sequential(
            nn.Conv2d(32, 64, 3, padding = 1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.Conv2d(64, 64, 3, padding = 1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.Dropout2d(p = 0.2)
        )

        self.block2 = nn.Sequential(
            nn.Conv2d(64, 64, 3, padding = 1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.Conv2d(64, 64, 3, padding = 1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.Dropout2d(p = 0.3)
        )

        self.identityConv = nn.Conv2d(64, 128, 1, stride = 2)

        self.block3 = nn.Sequential(
            nn.Conv2d(64, 128, 3, padding = 1, stride = 2),
            nn.BatchNorm2d(128),
            nn.ReLU(),
            nn.Conv2d(128, 128, 3, padding = 1),
            nn.BatchNorm2d(128),
            nn.ReLU(),
            nn.Dropout2d(p = 0.3)
        )

        self.block4 = nn.Sequential(
            nn.Conv2d(128, 128, 3, padding = 1),
            nn.BatchNorm2d(128),
            nn.ReLU(),
            nn.Conv2d(128, 128, 3, padding = 1),
            nn.BatchNorm2d(128),
            nn.ReLU()
        )

        self.pool = nn.Sequential(
            nn.AdaptiveAvgPool2d(4),
            nn.Dropout2d(p = 0.3)
        )

        self.linear = nn.Sequential(
            nn.Linear(128 * 4 * 4, 256),
            nn.ReLU(),
            nn.Dropout(p = 0.5),
            nn.Linear(256, 64),
            nn.ReLU(),
            nn.Dropout(p = 0.5),
            nn.Linear(64, 10),
        )

    def forward(self, x):
        x = self.begFilter(x)
        x = self.block1(x)
        residual = x
        x = self.block2(x)
        x += residual
        residual = x
        x = self.block3(x)
        x += self.identityConv(residual)
        residual = x
        x = self.block4(x)
        x += residual
        x = self.pool(x)
        x = x.view(-1, 128 * 4 * 4)
        x = self.linear(x)
        return x