import torch.nn as nn
import torch.nn.functional as F
class VggSm(nn.Module):
    def __init__(self):
        super(VggSm, self).__init__()
        self.cnn = nn.Sequential(
						nn.Conv2d(3, 32, 3, padding = 1),
						nn.ReLU(),
                        nn.Conv2d(32, 32, 3, padding = 1),
                        nn.ReLU(),
						nn.MaxPool2d(2, 2),
                        nn.Dropout2d(p = 0.2),
						nn.Conv2d(32, 64, 3, padding = 1),
                        nn.ReLU(),
                        nn.Conv2d(64, 64, 3, padding = 1),
						nn.ReLU(),
						nn.MaxPool2d(2, 2),
                        nn.Dropout2d(p = 0.2),
                        nn.Conv2d(64, 128, 3, padding = 1),
                        nn.ReLU(),
                        nn.Conv2d(128, 128, 3, padding = 1),
                        nn.ReLU(),
                        nn.Conv2d(128, 128, 3, padding = 1),
                        nn.ReLU(),
                        nn.LPPool2d(2, 2, 2),
                        nn.Dropout2d(p = 0.2)
						)
        self.linear = nn.Sequential(
            nn.Linear(128 * 4 * 4, 256),
            nn.ReLU(),
            nn.Dropout(p = 0.5),
            nn.Linear(256, 64),
            nn.ReLU(),
            nn.Dropout(p = 0.4),
            nn.Linear(64, 10)
        )

    def forward(self, x):
        x = self.cnn(x)
        x = x.view(-1, 128 * 4 * 4)
        x = self.linear(x)
        return x

                        # nn.Conv2d(64, 128, 4, padding = 2),
                        # nn.ReLU(),
                        # nn.Conv2d(128, 128, 4, padding = 1),
                        # nn.ReLU(),
                        # nn.Conv2d(128, 128, 4, padding = 2),
                        # nn.ReLU(),
                        # nn.LPPool2d(2, 2, 2),
                        # nn.Dropout2d(p = 0.2)