import torchvision
import torch.nn as nn

def create_vgg():
    vgg = torchvision.models.vgg13(pretrained = True)
    for param in vgg.parameters():
        param.requires_grad = False
    num_features = vgg.classifier[6].in_features
    features = list(vgg.classifier.children())[:-4]
    features.extend([
        nn.Linear(4096, 2048),
        nn.ReLU(),
        nn.Dropout(p = 0.4),
        nn.Linear(2048, 256),
        nn.ReLU(),
        nn.Dropout(p = 0.4),
        nn.Linear(256, 10)
    ])
    vgg.classifier = nn.Sequential(*features)
    return vgg