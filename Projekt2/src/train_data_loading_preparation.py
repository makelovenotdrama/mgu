import pandas as pd
import os
from os.path import isfile, join, isdir
from shutil import copyfile


in_path = './data/train'
out_path = './data/train_labeled'
labels_path = './data/trainLabels.csv'

x = pd.read_csv(labels_path)
labels_unique = x.label.unique()
if not isdir(out_path):
    os.mkdir(out_path)
for label in labels_unique:
    new_path = join(out_path, label)
    if not isdir(new_path):
        os.mkdir(new_path)
pngs = [f for f in os.listdir(in_path) if isfile(join(in_path, f))]
pngs
for png in pngs:
    id = int(png.replace('.png', ''))
    label = x.loc[x['id'] == int(id), 'label'].values[0]
    copyfile(join(in_path, png), join(out_path, label, png))



