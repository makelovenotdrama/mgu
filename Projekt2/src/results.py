import torch


def get_accuracy(net, valid_loader, device):
    correct = 0
    total = 0
    with torch.no_grad():
        for data in valid_loader:
            images, labels = data[0].to(device), data[1].to(device)
            outputs = net(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
            torch.cuda.empty_cache()
    return 100 * correct / total

def get_accuracy_classes(net, valid_loader, len_classes, device, batch_size):
    class_correct = list(0. for i in range(len_classes))
    class_total = list(0. for i in range(len_classes))
    with torch.no_grad():
        for data in valid_loader:
            images, labels = data[0].to(device), data[1].to(device)
            outputs = net(images)
            _, predicted = torch.max(outputs, 1)
            c = (predicted == labels).squeeze()
            for i in range(len(labels)):
                label = labels[i]
                class_correct[label] += c[i].item()
                class_total[label] += 1
    return [100 * class_correct[i] / class_total[i] for i in range(len_classes)]