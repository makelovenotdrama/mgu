import torch.optim as optim
import torch
import torch.nn as nn
from results import get_accuracy


def train(net, train_loader, valid_loader, num_epochs, lr, momentum, device, calculate_train_acc = False, use_scheduler = False, step_size = 15, gamma = 0.1):
    criterion = nn.CrossEntropyLoss().to(device, non_blocking = True)
    optimizer = optim.SGD(net.parameters(), lr=lr, momentum=momentum, nesterov=True)
    scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=step_size, gamma=gamma)
    accs_valid = []
    accs_train = []
    losses = []
    for epoch in range(num_epochs):  # loop over the dataset multiple times
        running_loss = 0.0
        net.train()
        for i, data in enumerate(train_loader, 0):
            inputs, labels = data[0].to(device), data[1].to(device)
            optimizer.zero_grad()
            outputs = net(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
            running_loss += loss.item()
        
        net.eval()
        valid_acc = get_accuracy(net, valid_loader, device)
        loss = running_loss / len(train_loader)
        if calculate_train_acc:
            train_acc = get_accuracy(net, train_loader, device)
            accs_train.append(train_acc)
            msg = f'epoch {epoch + 1}, valid  accuracy {valid_acc}, train accuracy {train_acc}, loss {loss}'
        else:
            msg = f'epoch {epoch + 1}, valid  accuracy {valid_acc}, loss {loss}'
        print(msg)
        if use_scheduler:
            scheduler.step()
        accs_valid.append(valid_acc)
        losses.append(loss)
        running_loss = 0.0
    return accs_valid, losses, accs_train