import os
from os.path import isfile, join, isdir
from shutil import copyfile

out_path = './data/test_prepared/test'
if not isdir('./data/test_prepared'):
    os.mkdir('./data/test_prepared')
if not isdir(out_path):
    os.mkdir(out_path)

in_path = './data/test'
pngs = [f for f in os.listdir(in_path) if isfile(join(in_path, f))]
pngs
for png in pngs:
    pnglen = len(png)
    zeros = '0'*(10 - pnglen)
    new_name = f'{zeros}{png}'
    copyfile(join(in_path, png), join(out_path, new_name))





