
import torch
import torchvision
import torchvision.transforms as transforms
from torch.utils.data import random_split
import numpy as np
from torch.utils.data.sampler import SubsetRandomSampler


def get_train_valid_loader(data_path, batch_size, resize = False, resize_size = 32, crop_size = 32):
    VALID_SIZE = 0.15
    TEST_SIZE = 0.15
    np.random.seed(seed=0)
    random_choice_transforms = [
        transforms.RandomRotation((0, 40)),
        transforms.RandomRotation((0, 40)),
        transforms.RandomResizedCrop(crop_size, scale = (0.7, 1), ratio = (0.85, 1.15)),
        transforms.RandomResizedCrop(crop_size, scale = (0.7, 1), ratio = (0.85, 1.15)),
        transforms.RandomRotation((0, 40)),
        transforms.RandomResizedCrop(crop_size, scale = (0.7, 1), ratio = (0.85, 1.15)),
        transforms.RandomResizedCrop(crop_size, scale = (0.7, 1), ratio = (0.85, 1.15)),
        transforms.RandomRotation((0, 40)),
        transforms.RandomVerticalFlip(p = 0),
        transforms.RandomVerticalFlip(p = 0)
    ]
    transfrorm_normalize = transforms.Normalize((0.4914, 0.4822, 0.4465), (0.247, 0.243, 0.261))

    if resize is True:
        train_transforms = transforms.Compose([
            transforms.Resize(resize_size),
            transforms.CenterCrop(crop_size),
            transforms.RandomChoice(random_choice_transforms),
            transforms.RandomHorizontalFlip(p = 0.5),
            transforms.RandomVerticalFlip(p = 0.1),
            transforms.ToTensor(),
            transfrorm_normalize])
        valid_transforms =  transforms.Compose([
            transforms.Resize(resize_size),
            transforms.CenterCrop(crop_size),
            transforms.ToTensor(),
            transfrorm_normalize])
    else:
        train_transforms = transforms.Compose([
            transforms.RandomChoice(random_choice_transforms),
            transforms.ToTensor(),
            transfrorm_normalize])
        valid_transforms =  transforms.Compose([
            transforms.ToTensor(),
            transfrorm_normalize])

    imported_train_dataset = torchvision.datasets.ImageFolder(
        root = data_path,
        transform = train_transforms
    )

    imported_valid_dataset = torchvision.datasets.ImageFolder(
        root = data_path,
        transform = valid_transforms
    )
    imported_test_dataset = torchvision.datasets.ImageFolder(
        root = data_path,
        transform = valid_transforms
    )
    classes = imported_train_dataset.classes

    num_train = len(imported_train_dataset)
    indices = list(range(num_train))
    valid_test_split = int(np.floor(VALID_SIZE * num_train))
    test_train_split = valid_test_split + int(np.floor(TEST_SIZE * num_train))
    np.random.shuffle(indices)
    train_idx = indices[test_train_split:]
    test_idx = indices[valid_test_split: test_train_split]
    valid_idx = indices[:valid_test_split]
    train_sampler = SubsetRandomSampler(train_idx)
    valid_sampler = SubsetRandomSampler(valid_idx)
    test_sampler = SubsetRandomSampler(test_idx)

    train_loader = torch.utils.data.DataLoader(
        imported_train_dataset, batch_size=batch_size, sampler=train_sampler,
        num_workers=2,
    )
    test_loader = torch.utils.data.DataLoader(
        imported_test_dataset, batch_size=batch_size, sampler=test_sampler,
        num_workers=2,
    )
    valid_loader = torch.utils.data.DataLoader(
        imported_valid_dataset, batch_size=batch_size, sampler=valid_sampler,
        num_workers=2
    )

    return train_loader, valid_loader, test_loader, classes
