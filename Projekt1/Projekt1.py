
# %%
import os
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from net import Net
from layer import Layer
from data_encoder import DataEncoder
from graph_creator import GraphCreator

# %%
def load_data(problem_type, encoder):
    if problem_type == "classification":
        train_data = np.genfromtxt("./data/classification/data.three_gauss.train.10000.csv",
                                    delimiter=',', skip_header=True, encoding="utf8")
        test_data = np.genfromtxt("./data/classification/data.three_gauss.test.1000.csv",
                                  delimiter=',', skip_header=True, encoding="utf8")
    elif problem_type == "regression":
        train_data = np.genfromtxt("./data/regression/data.activation.train.1000.csv",
                                   delimiter=',', skip_header=True, encoding="utf8")
        test_data = np.genfromtxt("./data/regression/data.activation.test.100.csv",
                                  delimiter=',', skip_header=True, encoding="utf8")

    train_x = train_data[:, :-1]
    test_x = test_data[:, :-1]
    train_y = train_data[:,-1:]

    if problem_type == "classification":
        train_values = np.array(train_data[:,-1])
        encoder.encode(train_values)
        train_y = encoder.encode(train_values)        

    test_y = test_data[:, -1:]
    if problem_type == "classification":
        test_values = np.array(test_data[:,-1])
        test_y = encoder.encode(test_values)
        
    return train_x.T, train_y.T, test_x.T, test_y.T

def build_net(layer_dims, cost_function, output_activation, hidden_activation, train_x, train_y, learning_rate, momentum_rate, num_iterations, 
            test_x, batch_size, bias_active, online, print_cost, print_interval, seed):
    layers = [Layer(layer_dims[0], None)]

    for i in range(1, len(layer_dims) - 1):
        layers.append(Layer(layer_dims[i], hidden_activation))
    layers.append(Layer(layer_dims[-1], output_activation))

    net = Net(layers)
    params, costs_train, costs_test = net.train_model(train_x, train_y, learning_rate, momentum_rate, cost_function, test_x, 
                                    num_iterations, batch_size, bias_active, online, print_cost=True, 
                                    print_interval = print_interval, seed =seed)
    return params, costs_train, costs_test, net

# Load data
encoder = DataEncoder()
creator = GraphCreator()
problem_type = "classification"
train_x, train_y, test_x, test_y = load_data(problem_type, encoder)

# Set model parameters
layers = [train_x.shape[0], 10, train_y.shape[0]]
hidden_activation = "tanh"
output_activation = "sigmoid"
cost_function = "crossentropy"
num_iterations = 300
learning_rate = 0.3
momentum_rate = 0.1
print_interval = 2
batch_size = 3000
online = False
bias_active = True
print_cost = True
show_input = True
show_error = True
show_result = True
seed = None

if show_input:
    if problem_type == "regression":
        creator.draw_data_regression(train_x[0, :], train_y[0, :], "Wykres danych treningowych")
    else: 
        creator.draw_data_classification(train_x, encoder.decode(train_y), "Wykres danych treningowych")

# Train model
params, costs_train, costs_test, net =  build_net(layers, cost_function, output_activation, hidden_activation, train_x, train_y, learning_rate, momentum_rate, num_iterations,
                                    test_x, batch_size, bias_active, online, print_cost, print_interval, seed)   

if show_error:
    creator.draw_output_error(train_y, test_y, encoder, print_interval, int(num_iterations / (train_x.shape[1] / batch_size)), problem_type, costs_train, costs_test)

if show_result:
    test_y = net.predict(test_x)
    if problem_type == "regression":
        creator.draw_data_regression(test_x[0, :], test_y[0, :], "Wykres wyników")
    else: 
        creator.draw_data_classification(test_x, encoder.decode(test_y), "Wykres wyników")



