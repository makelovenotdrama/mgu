import numpy as np

class DataEncoder():
    def __init__(self):
        self.unique_values = []

    def encode(self, values):
        results = []
        self.unique_values = sorted(list(set(values)))
        N = len(self.unique_values)

        for value in values:
            onehot = [0 for i in range(0, N)]
            index = self.unique_values.index(value)
            onehot[index] = 1
            results.append(onehot)
        
        ret = np.array([np.array(r) for r in results])
        return ret
    
    def decode(self, Y):
        result = []
        argmax = np.argmax(Y, axis=0)

        result = [self.unique_values[i] for i in argmax]

        return np.asarray(result)