import os
import numpy as np
import random
import matplotlib.pyplot as plt
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from factories.loss_factory import LossFactory

class Net():
    def __init__(self, layers):
        self.initialize_parameters(layers)

    def initialize_parameters(self, layers):
        self.parameters = {}
        self.L = len(layers) - 1

        for i in range(1, len(layers)):
            self.parameters['W' + str(i)] = np.random.randn(layers[i].dim, layers[i-1].dim) * 0.01
            self.parameters['b' + str(i)] = np.zeros([layers[i].dim, 1])
            self.parameters['activation' + str(i)] = layers[i].activation 
            self.parameters['prevDeltaW' + str(i)] = 0
            self.parameters['prevDeltab' + str(i)] = 0

    def train_model(self, X, Y, learning_rate, momentum_rate, cost_function,
                    test_x, num_iterations, batch_size = 100, bias_active=True, online = False, print_cost=True, print_interval=100, seed=None):
        self.cost_function = LossFactory().get_instance(cost_function)
        self.bias_active = bias_active
        init_seed = seed if seed != None else random.randint(1, 100000)
        print("Seed: {0}".format(init_seed))
        np.random.seed(init_seed)
        costs_train = []
        costs_test = []
        len_train = X.shape[1]
        batch_size_final = 1 if online is True else min(batch_size, len_train)
        num_epochs = int(num_iterations / (len_train / batch_size))

        for i in range(0, num_epochs):
            X_batched, Y_batched = self.create_batches(X, Y, batch_size_final, len_train)
            for X_mini, Y_mini in zip(X_batched, Y_batched):
                A, caches = self.model_forward(X_mini)
                grads = self.model_backward(A, Y_mini, caches)
                self.update_parameters(grads, learning_rate, momentum_rate)
            
            if print_cost and i % print_interval == 0:
                perc_train = self.predict(X)
                perc_test = self.predict(test_x)
                costs_train.append(perc_train)
                costs_test.append(perc_test)

        return self.parameters, costs_train, costs_test
    

    def create_batches(self, X, Y, batch_size, len_train):
        shuffler = np.random.permutation(len_train)
        X_permuted = X[:,shuffler]
        Y_permuted = Y[:,shuffler]
        X_batched = [X_permuted[:,k : k + batch_size] for k in range(0, len_train, batch_size)]
        Y_batched = [Y_permuted[:,k : k + batch_size] for k in range(0, len_train, batch_size)]
        return X_batched, Y_batched

    def predict(self, X):
        A, cache = self.model_forward(X)
        return A

    def model_forward(self, X):
        caches = []
        A = X

        # Hidden layers
        for i in range(1, self.L):
            A_prev = A
            A, cache = self.linear_activation_forward(A_prev, self.parameters['W' + str(i)], self.parameters['b' + str(i)], self.parameters['activation' + str(i)])
            caches.append(cache)

        # Output layer
        AL, cache = self.linear_activation_forward(A, self.parameters['W' + str(self.L)], self.parameters['b' + str(self.L)], self.parameters['activation' + str(self.L)])
        caches.append(cache)

        return AL, caches

    def linear_activation_forward(self, A, W, b, activation):
        Z = np.dot(W, A) + b
        linear_cache = (A, W, b)
        activation_cache = Z
        A = activation.base(Z)

        return A, (linear_cache, activation_cache)

    def update_parameters(self, grads, learning_rate, momentum_rate):

        for l in range(self.L):
            deltaW = - learning_rate * grads["dW" + str(l+1)] + momentum_rate * self.parameters['prevDeltaW' + str(l+1)]
            self.parameters["W" + str(l+1)] = self.parameters["W" + str(l+1)] + deltaW
            self.parameters['prevDeltaW' + str(l+1)] = deltaW

            deltab = - learning_rate * grads["db" + str(l+1)] + momentum_rate * self.parameters['prevDeltab' + str(l+1)]
            self.parameters["b" + str(l+1)] = self.parameters["b" + str(l+1)] + deltab
            self.parameters['prevDeltab' + str(l+1)] = deltab

    def model_backward(self, A, Y, caches):
        grads = {}
        Y = Y.reshape(A.shape)

        # Last layer
        current_cache = caches[self.L-1]
        grads["dA" + str(self.L-1)], grads["dW" + str(self.L)], grads["db" + str(self.L)] = self.linear_activation_backward(self.cost_function.prime(A, Y), current_cache, self.parameters['activation' + str(self.L)])

        # Rest of layers
        for i in reversed(range(self.L-1)):
            current_cache = caches[i]
            dA_prev_temp, dW_temp, db_temp = self.linear_activation_backward(grads["dA" + str(i+1)], current_cache, self.parameters['activation' + str(i+1)])
            grads["dA" + str(i)] = dA_prev_temp
            grads["dW" + str(i + 1)] = dW_temp
            grads["db" + str(i + 1)] = db_temp
        
        return grads

    def linear_activation_backward(self, dA, cache, activation):
        linear_cache, activation_cache = cache
        dZ = activation.prime(dA, activation_cache)
        dA, dW, db = self.linear_backward(dZ, linear_cache)
        return dA, dW, db

    def linear_backward(self, dZ, cache):
        A, W, b = cache
        m = A.shape[1]
        
        dW = 1/m* np.dot(dZ, A.T)
        db = 1/m* np.sum(dZ, axis=1, keepdims=True) if self.bias_active is True else 0
        dA = np.dot(W.T, dZ)

        return dA, dW, db




