
# %%
import os
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from net import Net
from layer import Layer
from data_encoder import DataEncoder
from error_counters.classifier_error_counter import ClassifierErrorCounter
from error_counters.regression_error_counter import RegressionErrorCounter
import pandas as pd

# %%
def load_data(encoder):
    train_data = np.genfromtxt("./data/mnist/train.csv",
                                    delimiter=',', skip_header=True, encoding="utf8")
    # test_data = np.genfromtxt("./data/mnist/test.csv",
    #                               delimiter=',', skip_header=True, encoding="utf8")
    train_x = train_data[:, 1:]
    train_x = train_x / 255
    # test_x = test_data
    train_y = train_data[:, 0]
    train_values = np.array(train_data[:,0])
    encoder.encode(train_values)
    train_y = encoder.encode(train_values)
    train_x_fin = train_x[:30000, :]
    test_x_fin = train_x[30000:42000, :]
    train_y_fin = train_y[:30000, :]
    test_y_fin = train_y[30000:42000, :]
    return train_x_fin.T, train_y_fin.T, test_x_fin.T, test_y_fin.T

def load_test_x():
    test_data = np.genfromtxt("./data/mnist/test.csv",
                               delimiter=',', skip_header=True, encoding="utf8")
    test_data = test_data.T
    return test_data / 255

def build_net(layer_dims, train_x, train_y, learning_rate, momentum_rate, num_iterations, 
            test_x, batch_size, online, print_cost, print_interval):
    cost_function = "crossentropy"
    output_activation = "sigmoid"
    layers = [Layer(layer_dims[0], None)]

    for i in range(1, len(layer_dims) - 1):
        layers.append(Layer(layer_dims[i], "relu"))
    layers.append(Layer(layer_dims[-1], output_activation))
    net = Net(layers)
    params, costs_train, costs_test = net.train_model(train_x, train_y, learning_rate, momentum_rate, cost_function, test_x,  num_iterations, batch_size, online, print_cost=True, print_interval = print_interval)
    return params, costs_train, costs_test, net

# Load data
encoder = DataEncoder()

train_x, train_y, test_x, test_y = load_data(encoder)
test_x_tr = load_test_x()

# Set model parameters
layers = [train_x.shape[0], 50, 15, train_y.shape[0]]
num_iterations = 12
learning_rate = 0.01
momentum_rate = 0.5
print_interval = 1
batch_size = 10
online = False
# Train model

params, costs_train, costs_test, net =  build_net(layers, train_x, train_y, learning_rate, momentum_rate, num_iterations,
                                    test_x, batch_size, online, True, print_interval)   


xlabel = [k for k in range(0, num_iterations, print_interval)]
acc_train = []
acc_test = []

decoded_train_y = encoder.decode(train_y)
decoded_test_y = encoder.decode(test_y)
err_counter = ClassifierErrorCounter()
for tr, tst in zip(costs_train, costs_test):
    dec_train = encoder.decode(tr)
    acc_train.append(err_counter.count_error(dec_train, decoded_train_y))
    dec_test = encoder.decode(tst)
    acc_test.append(err_counter.count_error(dec_test, decoded_test_y))

plt.plot(xlabel, acc_train)
plt.plot(xlabel, acc_test) 
plt.ylabel('% poprawnych klasyfikacji')
plt.xlabel('iteracje uczenia')
plt.legend(['zbiór treningowy', 'zbiór testowy'])
plt.grid()
plt.show()

test_y_tr = net.predict(test_x_tr)
decoded_test_tr = encoder.decode(test_y_tr)
dddd = list(range(1, decoded_test_tr.size + 1))
data_save = {'ImageId': dddd, 'Label': decoded_test_tr}
df = pd.DataFrame(data_save)
df.to_csv('data.csv', index=False, header=True)

# Predict
# labels, correctness = predict(params, train_x, train_y, problem_type, activation_function)
# print("Correctness for train set: " + str(correctness))
# labels, correctness = predict(params, test_x, test_y, problem_type, activation_function)
# print("Correctness for test set: " + str(correctness))


