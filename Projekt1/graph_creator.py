import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from error_counters.classifier_error_counter import ClassifierErrorCounter
from error_counters.regression_error_counter import RegressionErrorCounter

class GraphCreator():
    def draw_data_classification(self, X, Y, title):
        data = {"X": X[0, :], "Y": X[1, :], "cat": Y}
        df = pd.DataFrame(data)
        groups = df.groupby("cat")
        for name, group in groups:
            plt.plot(group["X"], group["Y"], marker="o", linestyle="", label=name)
        plt.legend()
        plt.title(title)
        plt.show()
    def draw_data_regression(self, X, Y, title):
        plt.plot(X, Y)
        plt.title(title)
        plt.grid()
        plt.show()
    def draw_output_error(self, train_y, test_y, encoder, print_interval, num_epochs, problem_type, costs_train, costs_test):
        xlabel = [k for k in range(0, num_epochs, print_interval)]
        acc_train = []
        acc_test = []

        if problem_type == "classification":
            decoded_train_y = encoder.decode(train_y)
            decoded_test_y = encoder.decode(test_y)
            err_counter = ClassifierErrorCounter()
            for tr, tst in zip(costs_train, costs_test):
                dec_train = encoder.decode(tr)
                acc_train.append(err_counter.count_error(dec_train, decoded_train_y))
                dec_test = encoder.decode(tst)
                acc_test.append(err_counter.count_error(dec_test, decoded_test_y))
        else:
            err_counter = RegressionErrorCounter()
            for tr, tst in zip(costs_train, costs_test):
                acc_train.append(err_counter.count_error(tr, train_y))
                acc_test.append(err_counter.count_error(tst, test_y))

        plt.plot(xlabel, acc_train)
        plt.plot(xlabel, acc_test) 
        plt.ylabel('% poprawnych klasyfikacji' if problem_type == 'classification' else 'błąd średniokwadratowy')
        plt.xlabel('Liczba epok')
        plt.legend(['Zbiór treningowy', 'Zbiór testowy'])
        plt.grid()
        plt.show()

