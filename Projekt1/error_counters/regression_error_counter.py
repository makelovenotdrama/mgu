from functions.mse import MSE
import numpy as np

class RegressionErrorCounter():
    def count_error(self, Y, Y_pred):
        return np.squeeze(MSE().base(Y, Y_pred))