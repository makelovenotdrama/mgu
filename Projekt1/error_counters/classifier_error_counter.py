class ClassifierErrorCounter():
    def count_error(self, Y, Y_pred):
        return (Y == Y_pred).sum() / float(Y.size) * 100.