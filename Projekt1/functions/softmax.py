import numpy as np

class Softmax():
    def base(self, Z):
        zet = Z - np.max(Z)
        sm = (np.exp(zet).T / np.sum(np.exp(zet), axis=0)).T
        return sm

    def prime(self, dA, activation_cache):
        Z = activation_cache
        s = self.base(Z).reshape(-1,1)
        return np.diagflat(s) - np.dot(s, s.T)