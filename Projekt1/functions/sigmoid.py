import numpy as np

class Sigmoid():
    def base(self, Z):
        return 1 / (1 + np.exp(-Z))

    def prime(self, dA, activation_cache):
        Z = activation_cache
        dZ = dA * self.base(Z) * self.base(1-Z)
        return dZ