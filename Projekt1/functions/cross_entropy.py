import numpy as np

class CrossEntropy():
    def base(self, A, Y):
        return -1 / Y.shape[1] * np.sum(np.multiply(Y, np.log(A)) + np.multiply((1-Y), np.log(1-A)))

    def prime(self, A, Y):
        return -(np.divide(Y, A) - np.divide(1 - Y, 1 - A))