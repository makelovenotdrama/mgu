import numpy as np

class Tanh():
    def base(self, Z):
        return np.tanh(Z)

    def prime(self, dA, activation_cache):
        Z = activation_cache
        dZ = dA * (1 - self.base(Z) * self.base(Z))
        return dZ