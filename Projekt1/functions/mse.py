import numpy as np

class MSE():
    def base(self, A, Y):
        return 1 / Y.shape[1] * np.sum(np.power(Y-A,2))
    
    def prime(self, A, Y):
        return 2 * (A - Y)