import numpy as np

class Relu():
    def base(self, Z):
        return np.maximum(Z, 0)

    def prime(self, dA, activation_cache):
        Z = activation_cache
        Z = np.heaviside(Z, 0)
        return dA * Z