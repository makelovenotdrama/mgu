from factories.activation_factory import ActivationFactory

class Layer():
    def __init__(self, dim, activation):
        self.dim = dim
        if activation is not None:
            self.activation = ActivationFactory().get_instance(activation)