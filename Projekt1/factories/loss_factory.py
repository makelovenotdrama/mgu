from functions.mse import MSE
from functions.cross_entropy import CrossEntropy

class LossFactory():
    def get_instance(self, name):
        instances = {
            'mse': MSE(),
            'crossentropy': CrossEntropy()
        }
        return instances.get(name)

