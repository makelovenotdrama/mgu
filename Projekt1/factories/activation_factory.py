from functions.identity import Identity
from functions.relu import Relu
from functions.sigmoid import Sigmoid
from functions.tanh import Tanh

class ActivationFactory():
    def get_instance(self, name):
        instances = {
            'identity': Identity(),
            'relu': Relu(),
            'sigmoid': Sigmoid(),
            'tanh': Tanh()
        }
        return instances.get(name)

