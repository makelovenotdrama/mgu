from shutil import copyfile
import os
from scipy.io import wavfile
import random
import numpy as np
import pandas as pd

SAMPLE_RATE = 16000
TRAIN_DF_PATH = "../data/train/train_list.csv"
TRAIN_DF_PATH2 = "../data/train/train_list2.csv"

dst_dir = "../data/train/audio"
os.makedirs(os.path.join(dst_dir, 'silence'), exist_ok=True)
noise_dir = "../data/train/audio/_background_noise_"
noise_files = [
    wavfile.read(os.path.join(noise_dir, file))
    for file in os.listdir(noise_dir)
    if file.endswith(".wav")
]
dicts = []
for i in range(10000):
    tracks = random.choices(noise_files, k=2)

    rate1, track1 = tracks[0]
    rate2, track2 = tracks[1]

    if rate1 != SAMPLE_RATE or rate2 != SAMPLE_RATE:
        continue

    rate = rate1

    minlen = min(int(len(track1) / rate), int(len(track2) / rate))
    start1 = random.randint(0, minlen - 2)
    start2 = random.randint(0, minlen - 2)
    track1 = track1[start1 * rate:(start1 + 1) * rate]
    track2 = track2[start2 * rate:(start2 + 1) * rate]

    track1_vol = random.random() % 0.4
    track2_vol = random.random() % 0.4
    track_mix = (track1 * track1_vol + track2 * track2_vol).astype(np.int16)
    filename = f'silence/{i}_silence.wav'

    filepath = os.path.join(dst_dir, filename)
    wavfile.write(filepath, rate, track_mix)
    new_dict = {'label': 'silence', 'path': filename}
    dicts.append(new_dict)
labels = pd.read_csv(TRAIN_DF_PATH)
new_pd = pd.DataFrame(dicts)
labels = labels.append(new_pd)
labels.to_csv(TRAIN_DF_PATH, index=False)
