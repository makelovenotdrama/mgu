import matplotlib.pyplot as plt

def draw_accuracy_train_valid(acc_valid, acc_train = None):
    xlabel = [k for k in range(1, len(acc_valid) + 1)]

    if acc_train is not None:
        plt.plot(xlabel, acc_train)
    plt.plot(xlabel, acc_valid) 
    plt.ylabel('% poprawnych klasyfikacji')
    plt.xlabel('Liczba epok uczenia')
    legend = ['Zbiór walidacyjny'] if acc_train is None else ['Zbiór treningowy', 'Zbiór walidacyjny']
    plt.legend(legend)
    plt.grid()
    plt.show()


def draw_loss(loss):
    xlabel = [k for k in range(1, len(loss) + 1)]

    plt.plot(xlabel, loss)
    plt.ylabel('Wartość funkcji straty')
    plt.xlabel('Liczba epok uczenia')
    plt.grid()
    plt.show()

def draw_accuracies_classes(classes_labels, classes_accuracies):
    fig = plt.figure()
    ax = fig.add_axes([0,0,1,1])
    ax.bar(classes_labels, classes_accuracies)
    plt.ylabel('% poprawnych klasyfikacji')
    plt.xticks(rotation=45)
    plt.show()