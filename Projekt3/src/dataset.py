import sys, os
import torch
import librosa
import numpy as np
import pandas as pd
from torch import Tensor
from scipy.io import wavfile
from torchvision import transforms
from torch.utils.data import DataLoader
from torch.utils.data.dataset import Dataset


class AudioDataSet(Dataset):
    def __init__(self, transform=None, mode="train"):
        # setting directories for data
        data_root = "../data"
        self.mode = mode

        self.labels_dict = self.create_dict()
        if self.mode is "train":
            self.data_dir = os.path.join(data_root, "train/audio")
            self.csv_file = pd.read_csv(os.path.join(data_root,"train/train_list.csv"))
        if self.mode is "validate":
            self.data_dir = os.path.join(data_root, "train/audio")
            self.csv_file = pd.read_csv(os.path.join(data_root,"train/validate_list.csv"))
        if self.mode is "traintest":
            self.data_dir = os.path.join(data_root, "train/audio")
            self.csv_file = pd.read_csv(os.path.join(data_root,"train/test_list.csv"))
        if self.mode is "test":
            self.data_dir = os.path.join(data_root, "test/audio")
            self.csv_file = pd.read_csv(os.path.join(data_root,"test/test_list.csv"))

        self.transform = transform
        
    def __len__(self):
        return self.csv_file.shape[0]

    def __getitem__(self, idx):
        filename = self.csv_file['path'][idx]
        rate, data = wavfile.read(os.path.join(self.data_dir, filename))

        if self.transform is not None:
            data = self.transform(data)

        if self.mode is not "test":
            label_name = self.csv_file['label'][idx]
            label = self.labels_dict[label_name] if label_name in self.labels_dict else self.labels_dict['unknown']
            return data, label

        else:
            return data

    def create_dict(self):
        labels = ["Yes", "No", "Up", "Down", "Left", "Right", "On", "Off", "Stop", "Go", "Silence", "Unknown"]
        labels = [label.lower() for label in labels]
        labels_dict = {}
        for i in range(0, len(labels)):
            labels_dict[labels[i]] = i

        return labels_dict
